# coding: utf-8
import numpy as np
import datetime
import matplotlib.dates as mdates
import scipy.io as sio
import csv
from collections import defaultdict
import matplotlib.pyplot as plt
import pickle
import sys
import pandas as pd
import os
import time
import pytz
plt.rcParams["figure.figsize"] = [12, 9]
plt.rcParams["axes.labelsize"]=12
plt.rcParams["axes.titlesize"]=16
plt.rcParams["xtick.labelsize"]=12
plt.rcParams["ytick.labelsize"]=12
plt.rcParams["legend.fontsize"]=11
plt.rcParams["font.size"]=13
plt.rcParams["savefig.bbox"]='tight'
import pdb
        
def round_to_multiple(x, base=5):
    return int(base * round(float(x)/base))
    
def read_tawes(path):
    # ==== Read data:
  tawes = dict()

  tmp = np.loadtxt(path, skiprows=1)

  # create dict with datetime as time index
  tt = tmp[:,0]
  tawes['time'] = []
  for ii, val in enumerate(tt):
      tawes['time'].append(datetime.datetime.utcfromtimestamp(int(val)))

  # ==== Get missing values:
  tmp[np.where(tmp <= -98.)] = np.nan

  tawes['dd'] = tmp[:,1]    # Windrichtung Grad
  tawes['ff'] = tmp[:,2]    # Windgeschwindigkeit m/s
  tawes['ffx'] = tmp[:,3]   # Windspitze m/s
  tawes['glo'] = tmp[:,4]   # Globalstrahlung W/m2
  tawes['p'] = tmp[:,5]     # Stationsdruck in hPa
  tawes['pred'] = tmp[:,6] # Stationsdruck reduziert auf MSL in hPa
  tawes['rf'] = tmp[:,7]    # relative Feuchte in %
  tawes['rftp'] = tmp[:,8] # relative Feuchte aus Taupunkt %
  tawes['rr'] = tmp[:,9]   # precipitation sum in 10 min
  tawes['tl'] = tmp[:,10]    # Lufttemperatur in Celsius
  tawes['tp'] = tmp[:,11]   # Taupunkt in Celsius
  tawes['ts'] = tmp[:,12]   # 5 cm Lufttemperatur in Celsius  

  # === convert to pandas dataframe 
  data = pd.DataFrame.from_dict(tawes)
  #data.set_index('time',inplace=True)

  return data    
    

def roundTime(dt=None, roundTo=60):
    """Round a datetime object to any time laps in seconds
    dt : datetime.datetime object, default now.
    roundTo : Closest number of seconds to round to, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
    """
    if dt == None : dt = datetime.datetime.now()
    seconds = (dt.replace(tzinfo=None) - dt.min).seconds
    rounding = (seconds+roundTo/2) // roundTo * roundTo
    return dt + datetime.timedelta(0,rounding-seconds,-dt.microsecond)


### settings ####################################
num_hours = float(sys.argv[1]) 
# find start and ending index for desired time period
if len(sys.argv)==3:
    now_date = roundTime(datetime.datetime.strptime(sys.argv[3], '%Y%m%d%H'), 3600)
else:
    now_date = roundTime(datetime.datetime.utcnow(),3600)
bdy_date = now_date - datetime.timedelta(hours=num_hours)

stat_NR = [8989037, 8989038, 8989036, 8989035, 8989034, 8989033, 11126]
stat_height = [611, 635,     668,     920,     1204,    1556,    2252]
stat_name = ['Hauptbahnhof', 'O-Dorf', 'Alpenzoo', 'Hungerburg', 'Rastlboden', 'Hoettinger Alm', 'Patscherkofel']
colors = ["black","#C28500","#C86DD7", "grey","#00AB66","black","#5093E3"]

styles = ['dashed', 'dashed','dashed','dashed','dashed','dashed','dashed']
dash_styles= [(8,2), (2,2), (5,2), (3,3), (7,1), (2,1), (4,1)]
path_to_data = '/home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/'

save_fig = 1
figure_path = '/mnt/PIANO_campaign/weather_stations/ZAMG_slope_profile/'

### load data
data = []
for i, val in enumerate(stat_NR):
    if val < 8000000:
        path = path_to_data+'tawes_'+str(val)+'.dat'
    else:
        path = path_to_data+'tempis_'+str(val)+'.dat'
    
    print 'Reading data ... '+path
    data.append(read_tawes(path))

print 'Data reading successful'

# set date ticks
#now_mdate = mdates.date2num(now_date)
#if (now_mdate%1) >= 0 and (now_mdate%1) < 0.125: # between 0 and 3 UTC
#    last_mdate = np.floor(now_mdate)+0.125 # set last tick to 3 UTC
#elif now_mdate%1 >= 0.125 and now_mdate%1 < 0.25:
#    last_mdate = np.floor(now_mdate)+0.25
#elif now_mdate%1 >= 0.25 and now_mdate%1 < 0.375:
#    last_mdate = np.floor(now_mdate)+0.375
#elif now_mdate%1 >= 0.375 and now_mdate%1 < 0.5:
#    last_mdate = np.floor(now_mdate)+0.5
#elif now_mdate%1 >= 0.5 and now_mdate%1 < 0.625:
#    last_mdate = np.floor(now_mdate)+0.625
#elif now_mdate%1 >= 0.625 and now_mdate%1 < 0.75:
#    last_mdate = np.floor(now_mdate)+0.75
#elif now_mdate%1 >= 0.75 and now_mdate%1 < 0.875:
#    last_mdate = np.floor(now_mdate)+0.875
#elif now_mdate%1 >= 0.875:
#    last_mdate = np.floor(now_mdate)+1.0
#date_list = [mdates.num2date(last_mdate) - datetime.timedelta(hours=x) for x in np.arange(0, num_hours+6, 6)]



# trim dataframe to specific period
for iii, val in enumerate(stat_NR):
    mask = (data[iii]['time'] >= bdy_date) & (data[iii]['time'] <= now_date)
    data[iii] = data[iii].loc[mask]
    data[iii].set_index('time',inplace=True)

Tmin = np.min(data[0]['tl'])
Tmax = np.max(data[0]['tl'])
### plotting

# open figure
fig = plt.figure(figsize=(12,10)) 
plt.subplot(1, 2, 1)
ax1=[]
for iii, val in enumerate(stat_NR):
    ax1.append(data[iii]['tl'].plot(lw=1.5, ls = styles[iii], dashes=dash_styles[iii], color = colors[iii], label = stat_name[iii]+'('+str(stat_height[iii])+'m)'))
    if np.min(data[iii]['tl']) < Tmin:
        Tmin = np.min(data[iii]['tl'])
        min_index = iii
    if np.max(data[iii]['tl']) > Tmax:
        Tmax = np.max(data[iii]['tl'])
        max_index = iii        
        
ax1[0].set_ylabel(u'Temperature (°C)', labelpad=8)
ax1[0].set_xlabel(' ')
Tmin = (Tmin - Tmin%2)
Tmax = (Tmax - Tmax%2)+2
T_ticks = np.arange(Tmin, Tmax+1) # create tick array
for i, val in enumerate(stat_NR):
    ax1[i].set_yticks(T_ticks[::2])
    ax1[i].set_yticks(T_ticks[1::2], minor=True)
    ax1[i].set_ylim((Tmin, Tmax))
    # set dateticks
    #ax1[i].set_xticks(date_list)

    #set grid
    ax1[i].grid(which = 'minor', ls='dashed')
    ax1[i].grid(which = 'major', ls='solid')
t_start = pd.to_datetime(data[0].index.values[0]) 
t_end = pd.to_datetime(data[0].index.values[-1]) 
plt.gcf().text(0.5,0.96,'Temperature and relative humidity from '+ t_start.strftime('%Y.%m.%d %H:%M')+ ' to ' + t_end.strftime('%Y.%m.%d %H:%M'), ha='center')
# print legend
plt.legend(ncol=2, fontsize=10, handlelength=3.6)

plt.subplot(1, 2, 2)
ax2=[]
for iii, val in enumerate(stat_NR):
    ax2.append(data[iii]['rf'].plot(lw=1.5, ls = styles[iii], dashes=dash_styles[iii], color = colors[iii], label = stat_name[iii]+'('+str(stat_height[iii])+'m)'))  
ax2[0].set_xlabel(' ')      
ax2[0].set_ylabel(u'Relative humidity (%)', labelpad=4)
RH_ticks = np.arange(0,105,5) # create tick array
for i, val in enumerate(stat_NR):
    ax2[i].set_yticks(RH_ticks[::2])
    ax2[i].set_yticks(RH_ticks[1::2], minor=True)
    ax2[i].set_ylim((0, 100))
    # set dateticks
    #ax2[i].set_xticks(date_list)
    
    #set grid
    ax2[i].grid(which = 'minor', ls='dashed')
    ax2[i].grid(which = 'major', ls='solid')

# print legend
plt.legend(ncol=2, fontsize=10, handlelength=3.6)


plt.tight_layout()

plt.subplots_adjust(hspace=0.01, top=0.94) 

if save_fig == 1:
    if not os.path.exists(figure_path):
        os.makedirs(figure_path)
    plt.savefig(figure_path + 'slope_profile_'+t_start.strftime('%Y%m%d%H%M')+'_'+t_end.strftime('%Y%m%d%H%M')+'.png', bbox_inches='tight')
    print('Saved file')
else:
    plt.show()
    pdb.set_trace()

