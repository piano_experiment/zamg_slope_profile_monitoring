#/bin/bash -ex

while true; do
	# get YYYYmmdd timestamp for current day
	#endDay=$(/bin/date +%Y%m%d)
	endDay=$(/bin/date --date='+1 day' +%Y%m%d)
	# get YYYYmmdd timestamp for start (-3 days by default)
	startDay=$(/bin/date --date='-4 day' +%Y%m%d)

	echo "Fetching data for Hoettingeralm from $startDay to $endDay"
        ssh acinn_sybase@meteo-data "cd ~/Sybase2; ../virtualenv/bin/python getobs.py -t tawes -s 8989033 --YYYYmmdd -a -b $startDay -e $endDay -c dd,ff,ffx,glo,p,pred,rf,rftp,tl,tp,ts,rr" > tmp.dat
	# remove comments from fetched data in order to read it with python
	echo "Formatting file"
	grep -v "#" tmp.dat > /home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/tempis_8989033.dat 
	rm tmp.dat

        echo "Fetching data for Rastlboden from $startDay to $endDay"
        ssh acinn_sybase@meteo-data "cd ~/Sybase2; ../virtualenv/bin/python getobs.py -t tawes -s 8989034 --YYYYmmdd -a -b $startDay -e $endDay -c dd,ff,ffx,glo,p,pred,rf,rftp,tl,tp,ts,rr" > tmp.dat
        # remove comments from fetched data in order to read it with python
        echo "Formatting file"
        grep -v "#" tmp.dat > /home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/tempis_8989034.dat    
        rm tmp.dat

        echo "Fetching data for Hungerburg from $startDay to $endDay"
        ssh acinn_sybase@meteo-data "cd ~/Sybase2; ../virtualenv/bin/python getobs.py -t tawes -s 8989035 --YYYYmmdd -a -b $startDay -e $endDay -c dd,ff,ffx,glo,p,pred,rf,rftp,tl,tp,ts,rr" > tmp.dat
        # remove comments from fetched data in order to read it with python
        echo "Formatting file"
        grep -v "#" tmp.dat > /home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/tempis_8989035.dat
        rm tmp.dat

        echo "Fetching data for Alpenzoo from $startDay to $endDay"
        ssh acinn_sybase@meteo-data "cd ~/Sybase2; ../virtualenv/bin/python getobs.py -t tawes -s 8989036 --YYYYmmdd -a -b $startDay -e $endDay -c dd,ff,ffx,glo,p,pred,rf,rftp,tl,tp,ts,rr" > tmp.dat
        # remove comments from fetched data in order to read it with python
        echo "Formatting file"
        grep -v "#" tmp.dat > /home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/tempis_8989036.dat    
        rm tmp.dat

        echo "Fetching data for Hauptbahnhof from $startDay to $endDay"
        ssh acinn_sybase@meteo-data "cd ~/Sybase2; ../virtualenv/bin/python getobs.py -t tawes -s 8989037 --YYYYmmdd -a -b $startDay -e $endDay -c dd,ff,ffx,glo,p,pred,rf,rftp,tl,tp,ts,rr" > tmp.dat
        # remove comments from fetched data in order to read it with python
        echo "Formatting file"
        grep -v "#" tmp.dat > /home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/tempis_8989037.dat
        rm tmp.dat

        echo "Fetching data for O-Dorf from $startDay to $endDay"
        ssh acinn_sybase@meteo-data "cd ~/Sybase2; ../virtualenv/bin/python getobs.py -t tawes -s 8989038 --YYYYmmdd -a -b $startDay -e $endDay -c dd,ff,ffx,glo,p,pred,rf,rftp,tl,tp,ts,rr" > tmp.dat
        # remove comments from fetched data in order to read it with python
        echo "Formatting file"
        grep -v "#" tmp.dat > /home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/tempis_8989038.dat
        rm tmp.dat

        echo "Fetching data for Patscherkofel from $startDay to $endDay"
        ssh acinn_sybase@meteo-data "cd ~/Sybase2; ../virtualenv/bin/python getobs.py -t tawes -s 11126 --YYYYmmdd -a -b $startDay -e $endDay -c dd,ff,ffx,glo,p,pred,rf,rftp,tl,tp,ts,rr" > tmp.dat
        # remove comments from fetched data in order to read it with python
        echo "Formatting file"
        grep -v "#" tmp.dat > /home/lukas/PIANO_folder/ObsData/ZAMG_slope_profile/tawes_11126.dat
        rm tmp.dat

	# plotting data
	source activate WRFpython
	python /media/lukas/Data/PIANO/ObsData/ZAMG_slope_profile/plot_ZAMG_slope_profile.py 60

	#wait for one hour
	echo "Done with plotting, wating for 3h"
	sleep 3h
	echo "Waiting time expired"
done

